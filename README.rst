
.. image:: setlists-logo.png
   :width: 214
   :alt: Setlists


**Setlists** is a command line tool to retrieve user setlist information
from the **setlist.fm** web API. An API key is required.

Private information is stored in a ``secrets.py`` file and imported at run time.
Create it and add a user name and key as in the following example:

.. code-block:: python

    setlist_user = 'your_username'
    setlist_key = 'your_setlist_api_key'


This file is **not** included in the repository. Make sure to add the file to your .gitignore!



* Free software: MIT license
* Documentation: https://swtools.readthedocs.io.


Features
--------
* Display attended concert information.
* Save full event data including complete setlists.


Task List
---------
* Use locally cached results to reduce API calls.



Credits
-------

**setlist.fm** API `documentation`_.

.. _`documentation`: https://api.setlist.fm/docs/1.0/index.html

Apply for an API `key`_ (registration required).

.. _`key`: https://www.setlist.fm/settings/apps

