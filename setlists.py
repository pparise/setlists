#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Get User Setlists from Setlist.fm.

Apply for an API key (registration required)
https://www.setlist.fm/settings/apps

The setlist.fm API docs
https://api.setlist.fm/docs/1.0/index.html

I added a wait between page calls to avoid the rate limit
or else you get a 429 error (Too Many Requests)

The rate limit is max. 2.0/second and max. 1440/DAY

v1.0.0 - Original version
v1.1.0 - Code refactoring and documentation
v1.2.0 - Move private info to external file and read at run time
v1.2.1 - Change private info to secrets file and import

"""

__author__ = "pparise"
__version__ = "1.2.1"
__license__ = "MIT"

# standard library modules
import os
import sys
import json
import math
import time
import datetime

# 3rd party modules
import requests

# local modules
import secrets


# setlist.fm user
user = secrets.setlist_user

# setlist.fm API key
key = secrets.setlist_key

# request header
headers = {'Accept': 'application/json', 'x-api-key': key}


def main():
    """Main entry point of the app.

    Retrieve, display, and save attended setlist concert data for a user.
    Only the attended concerts are displayed.
    The saved JSON contains the full event data including complete setlists.
    """

    # the url for the API resource, starts at page 1
    url = 'https://api.setlist.fm/rest/1.0/user/' + user + '/attended?p=1'

    # get the first page of data
    response = get_page(url)

    # load the JSON response into a dictionary
    data = json.loads(response.text)

    # at this point we have the first page of setlists, let's print it
    print_data(data)

    # and then save the response to a file
    save_data(data, 1)

    # we can also figure out the number of pages (adding 1 for use with range function)
    pagesize = data['itemsPerPage']
    setlists = int(data['total'])
    pages = int(math.ceil(setlists / pagesize) + 1)

    # get every other page starting at 2
    for page in range(2, pages):

        # set a delay to avoid the rate limit
        time.sleep(3)

        # the url for the API resource
        url = 'https://api.setlist.fm/rest/1.0/user/' + user + '/attended?p=' + str(page)

        # get a page of data
        response = get_page(url)

        # load the JSON response into a dictionary
        data = json.loads(response.text)

        # print a page of data to the screen
        print_data(data)

        # save the response to a file
        save_data(data, page)

        # we are done!


def save_data(data, page):
    """Save the JSON response to a file.

    This saves the full JSON response not just what is displayed on screen.

    :param data: dictionary with the JSON response
    :type data: dict

    :param page: page number
    :type: int
    """

    filename = 'setlist.fm-' + user + '-p' + str(page) + '.json'
    fullpath = os.path.join(os.path.dirname(__file__), 'data/' + filename)

    with open(fullpath, mode='w', encoding='UTF-8') as file:
        json.dump(data, file, ensure_ascii=False, indent=4)


def print_data(data):
    """Print data to the console.

    There are many more available fields, customize as required.

    Be careful of handling missing keys!

    :param data: dictionary with the JSON response
    :type data: dict
    """

    for setlist in data['setlist']:
        date = iso_date(str(setlist['eventDate']))
        artist = setlist['artist']['name']
        venue = setlist['venue']['name']
        city = setlist['venue']['city']['name']
        where = '(' + venue + ', ' + city + ')'
        if 'tour' in setlist:
            tour = '[' + setlist['tour']['name'] + ']'
        else:
            tour = ''
        print(date, artist, where, tour)


def get_page(url):
    """Make an API call.

    Handles the most common exceptions.

    :param url: full url for the API resource (including page number)
    :type url: str

    :returns: JSON response
    :rtype: obj
    """

    try:
        response = requests.get(url, headers=headers, timeout=20)
        response.raise_for_status()
    except requests.exceptions.HTTPError as errh:
        print('HTTP Error:', errh)
        sys.exit(1)
    except requests.exceptions.ConnectionError as errc:
        print('Connection Error:', errc)
        sys.exit(1)
    except requests.exceptions.Timeout as errt:
        print('Timeout Error:', errt)
        sys.exit(1)
    except requests.exceptions.RequestException as err:
        print('OOps Unknown Error:', err)
        sys.exit(1)

    return response


def iso_date(date):
    """Format date as ISO8601.

    Currently the setlist API returns the date in US style format (dd-MM-yyyy).

    Convert it to the unambiguous ISO8601 format (YYYY-MM-DD).

    :param date: a string representing a date
    :type: str

    :returns: a string representing a date in the new format
    :rtype: str
    """

    date1 = datetime.datetime.strptime(date, '%d-%m-%Y')
    return datetime.datetime.strftime(date1, '%Y-%m-%d')


if __name__ == '__main__':
    main()
